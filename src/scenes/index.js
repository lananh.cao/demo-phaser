import LoadScene from './loadScene';
import MenuScene from './menuScene';
import GameScene from './gameScene';

export {
  LoadScene,
  MenuScene,
  GameScene
};
