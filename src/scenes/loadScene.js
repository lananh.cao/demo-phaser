import {scene} from '../scene';
import MenuScene from './menuScene';

export default class LoadScene extends Phaser.Scene {
  constructor() {
    super({
      key: scene.SCENES.LOAD
    });
  }

  init() {}

  preload() {
    this.load.image('bg', './assets/background.png');
    this.load.image('forest_bg', './assets/forest_bg.png');
    this.load.image('star', './assets/star.png');
    this.load.image('play-button', './assets/play.png');
    this.load.image('option-button', './assets/option.png');
    
    this.load.spritesheet('dude', './assets/dude.png', {
      frameWidth: 32, frameHeight: 48
    });
      
    this.load.audio('music', './assets/Winding-Down_Looping.mp3');

    let loadingBar = this.add.graphics({
      fillstyle: {
        color: 0xffffff
      }
    });

    for (let i = 0; i < 100; i++) {
      this.load.spritesheet('dude' + i, './assets/dude.png', {
        frameHeight: 48,
        frameWidth: 32
      });
    }

    this.load.on('progress', (percent) => {
      loadingBar.fillRect(0, this.game.renderer.height / 2, this.game.renderer.width * percent, 50);
      console.log(percent);
    });

    this.load.on('complete', () => {
    });
  }

  create() {
    this.scene.start(scene.SCENES.MENU);
  }
}
