import {scene} from '../scene';

export default class MenuScene extends Phaser.Scene {
  constructor() {
    super({
      key: scene.SCENES.MENU
    });
  }
  init() {}

  preload() {

  }
  create() {
    this.myBG = this.add.image(0, 0, 'bg').setOrigin(0).setDepth(0);
    this.myBG.setDisplaySize(this.myBG.width * 2, this.myBG.height * 1.2)

    let playButton = this.add.image(this.game.renderer.width / 2, this.game.renderer.height / 2 - 100, 'play-button').setDepth(1);

    let optionButton = this.add.image(this.game.renderer.width / 2, this.game.renderer.height / 2 + 100, 'option-button').setDepth(1);

    let hoverSprite = this.add.sprite(100, 100, 'dude');
    hoverSprite.setScale(1);
    hoverSprite.setVisible(false);

    this.anims.create({
      key: 'walk',
      frameRate: 4,
      repeat: -1,
      frames: this.anims.generateFrameNumbers('dude', {
        frames: [0, 1, 2, 3]
      })
    });

    this.sound.pauseOnBlur = false;
    this.sound.play('music', {
      loop: true
    });

    playButton.setInteractive();
    playButton.on('pointerover', () => {
      hoverSprite.setVisible(true);
      hoverSprite.play('walk');
      hoverSprite.x = playButton.x - playButton.width + 120;
      hoverSprite.y = playButton.y;
    });
    playButton.on('pointerout', () => {
      hoverSprite.setVisible(false);
    });
    playButton.on('pointerup', () => {
      this.scene.start(scene.SCENES.PLAY);
    });

    optionButton.setInteractive();
    optionButton.on('pointerover', () => {
      hoverSprite.setVisible(true);
      hoverSprite.play('walk');
      hoverSprite.x = optionButton.x - optionButton.width + 120;
      hoverSprite.y = optionButton.y;
    });
    optionButton.on('pointerout', () => {
      hoverSprite.setVisible(false);
    });
  }
}
