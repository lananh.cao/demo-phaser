export const scene = {
  SCENES: {
    LOAD: 'LOAD',
    MENU: 'MENU',
    PLAY: 'PLAY'
  },
  IMAGE: {
    OPTIONS: 'option.png',
    PLAY: 'play.png'
  },
  AUDIO: {
    TITLE: 'Winding-Down_Looping.mp3'
  },
  SPRITE: {
    DUDE: 'dude.png'
  }
};
