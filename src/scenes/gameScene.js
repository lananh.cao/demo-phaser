import Phaser from 'phaser';
import {
  scene
} from '../scene';

var playerDude;
var key;

export default class GameScene extends Phaser.Scene {
  constructor() {
    super({
      key: scene.SCENES.PLAY
    });
  }
  init() {}

  preload() {
    this.load.image('tiles', 'assets/tiles_001.png');
    this.load.tilemapTiledJSON('tile-map', 'assets/map.json');
  }

  create() {
    this._myBG = this.add.image(0, 0, 'forest_bg').setOrigin(0).setDepth(0);
    this._myBG.setDisplaySize(this._myBG.width, this._myBG.height);

    const map = this.make.tilemap({
      key: 'tile-map'
    });

    const tiles = map.addTilesetImage('tiles_001', 'tiles');

    const layer = map.createStaticLayer(0, tiles, 0, 0);

    layer.setCollisionBetween(0, 30);

    playerDude = this.physics.add.sprite(100, 10, 'dude');

    playerDude.setBounce(0.2);
    playerDude.setCollideWorldBounds(true);

    this.anims.create({
      key: 'left',
      frames: this.anims.generateFrameNumbers('dude', {
        start: 0, end: 3
      }),
      frameRate: 10,
      repeat: -1
    });

    this.anims.create({
      key: 'turn',
      frames: [ {
        key: 'dude', frame: 4
      } ],
      frameRate: 20
    });
    this.anims.create({
      key: 'right',
      frames: this.anims.generateFrameNumbers('dude', {
        start: 5, end: 8
      }),
      frameRate: 10,
      repeat: -1
    });

    playerDude.body.setGravityY(500);

    this.physics.add.collider(playerDude, layer);

    key = this.input.keyboard.addKeys('A,D, SPACE');
  }

  update() {
    if (key.A.isDown){
      playerDude.setVelocityX(-160);
      playerDude.anims.play('left', true);
    }
    else if (key.D.isDown){
      playerDude.setVelocityX(160);
      playerDude.anims.play('right', true);
    } 
    else if (key.SPACE.isDown) {
      playerDude.setVelocityY(-200);
      playerDude.anims.play(key.A.isDown ? 'left':'right', true);   
    }
    else {
      playerDude.setVelocityX(0);
      playerDude.anims.play('turn', true);
    }
  }
}
