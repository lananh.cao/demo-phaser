import * as Scenes from '../scenes';

const config = {
  width: window.innerWidth * window.devicePixelRatio,
  height: window.innerHeight * window.devicePixelRatio,
  scene: [
    Scenes.LoadScene,
    Scenes.MenuScene,
    Scenes.GameScene
  ],
  render: {
    pixelArt: true
  },
  physics: {
    default: 'arcade',
    arcade: {
      gravity: {
        y: 100
      },
      debug: true
    }
  }
};

export default config;
